import type { StoryObj } from '@storybook/react';
import { Button } from 'ui';

export default {
	title: 'UI/Button',
	component: Button,
	// This component will have an automatically generated Autodocs entry: https://storybook.js.org/docs/writing-docs/autodocs
	tags: ['autodocs'],
	parameters: {
		// More on how to position stories at: https://storybook.js.org/docs/configure/story-layout
		layout: 'centered',
	},
};

type Story = StoryObj<typeof Button>;

export const PrimaryButton: Story = {
	parameters: {
		docs: {
			description: {
				story: 'Button style that indicates primary button',
			},
		}
	},
	args: {
		Text: 'Primary Button',
		Feeling: 'primary',
		OnClick: () => alert('button clicked')
	}
};

export const SecondaryButton: Story = {
	parameters: {
		docs: {
			description: {
				story: 'Button style that indicates secondary button',
			},
		}
	},
	args: {
		Text: 'Secondary Button',
		Feeling: 'secondary',
		OnClick: () => alert('button clicked')
	}
};

export const PositiveButton: Story = {
	parameters: {
		docs: {
			description: {
				story: 'Button style that indicates something good happening',
			},
		}
	},
	args: {
		Text: 'Button',
		Feeling: 'positive',
		OnClick: () => alert('button clicked')
	}
};

export const NegativeButton: Story = {
	parameters: {
		docs: {
			description: {
				story: 'Button style that indicates something bad or dangerous happening',
			},
		}
	},
	args: {
		Text: 'Button',
		Feeling: 'negative',
		OnClick: () => alert('button clicked')
	}
};