import type { StoryObj } from '@storybook/react';
import { Checkbox } from 'ui';

export default {
	title: 'FORM/Checkbox',
	component: Checkbox,
	// This component will have an automatically generated Autodocs entry: https://storybook.js.org/docs/writing-docs/autodocs
	tags: ['autodocs'],
	parameters: {
		// More on how to position stories at: https://storybook.js.org/docs/configure/story-layout
		layout: 'centered',
	},
};

type Story = StoryObj<typeof Checkbox>;

export const BasicStory: Story = {
	args: {
		UniqueName: 'checkbox-1',
		Label: 'I like carrots'
	}
};
