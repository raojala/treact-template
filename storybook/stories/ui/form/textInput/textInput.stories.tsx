import type { StoryObj } from '@storybook/react';
import { TextInput } from 'ui';

export default {
	title: 'FORM/TextInput',
	component: TextInput,
	// This component will have an automatically generated Autodocs entry: https://storybook.js.org/docs/writing-docs/autodocs
	tags: ['autodocs'],
	parameters: {
		// More on how to position stories at: https://storybook.js.org/docs/configure/story-layout
		layout: 'centered',
	},
};

type Story = StoryObj<typeof TextInput>;

export const BasicStory: Story = {
	args: {
		UniqueName: 'text-input-1',
		Label: 'Text',
		Placeholder: 'placeholder'
	}
};
