import type { StoryObj } from '@storybook/react';
import { NumberInput } from 'ui';

export default {
	title: 'FORM/NumberInput',
	component: NumberInput,
	// This component will have an automatically generated Autodocs entry: https://storybook.js.org/docs/writing-docs/autodocs
	tags: ['autodocs'],
	parameters: {
		// More on how to position stories at: https://storybook.js.org/docs/configure/story-layout
		layout: 'centered',
	},
};

type Story = StoryObj<typeof NumberInput>;

export const BasicStory: Story = {
	args: {
		UniqueName: 'numberbox-1',
		Label: 'number'
	}
};
