import type { StoryObj } from '@storybook/react';
import { TextArea } from 'ui';

export default {
	title: 'FORM/TextArea',
	component: TextArea,
	// This component will have an automatically generated Autodocs entry: https://storybook.js.org/docs/writing-docs/autodocs
	tags: ['autodocs'],
	parameters: {
		// More on how to position stories at: https://storybook.js.org/docs/configure/story-layout
		layout: 'centered',
	},
};

type Story = StoryObj<typeof TextArea>;

export const BasicStory: Story = {
	args: {
		UniqueName: 'textarea-1',
		Label: 'text',
		Placeholder: 'placeholder text'
	}
};
