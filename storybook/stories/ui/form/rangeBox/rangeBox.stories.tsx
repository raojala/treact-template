import type { StoryObj } from '@storybook/react';
import { RangeInput } from 'ui';

export default {
	title: 'FORM/RangeInput',
	component: RangeInput,
	// This component will have an automatically generated Autodocs entry: https://storybook.js.org/docs/writing-docs/autodocs
	tags: ['autodocs'],
	parameters: {
		// More on how to position stories at: https://storybook.js.org/docs/configure/story-layout
		layout: 'centered',
	},
};

type Story = StoryObj<typeof RangeInput>;

export const BasicStory: Story = {
	args: {
		UniqueName: 'RangeInput-1',
		Label: 'range-1',
		Min: 0,
		Max: 10
	}
};
