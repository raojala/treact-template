import type { StoryObj } from '@storybook/react';
import { PasswordInput } from 'ui';

export default {
	title: 'FORM/PasswordInput',
	component: PasswordInput,
	// This component will have an automatically generated Autodocs entry: https://storybook.js.org/docs/writing-docs/autodocs
	tags: ['autodocs'],
	parameters: {
		// More on how to position stories at: https://storybook.js.org/docs/configure/story-layout
		layout: 'centered',
	},
};

type Story = StoryObj<typeof PasswordInput>;

export const BasicStory: Story = {
	args: {
		UniqueName: 'passwordbox-1',
		Label: 'Password'
	}
};
