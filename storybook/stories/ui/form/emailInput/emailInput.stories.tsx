import type { StoryObj } from '@storybook/react';
import { EmailInput } from 'ui';

export default {
	title: 'FORM/EmailInput',
	component: EmailInput,
	// This component will have an automatically generated Autodocs entry: https://storybook.js.org/docs/writing-docs/autodocs
	tags: ['autodocs'],
	parameters: {
		// More on how to position stories at: https://storybook.js.org/docs/configure/story-layout
		layout: 'centered',
	},
};

type Story = StoryObj<typeof EmailInput>;

export const BasicStory: Story = {
	args: {
		UniqueName: 'email-input-1',
		Label: 'I like carrots'
	}
};
