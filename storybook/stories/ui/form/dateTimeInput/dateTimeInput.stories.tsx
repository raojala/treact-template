import type { StoryObj } from '@storybook/react';
import { DateTimeInput } from 'ui';

export default {
	title: 'FORM/DateTimeInput',
	component: DateTimeInput,
	// This component will have an automatically generated Autodocs entry: https://storybook.js.org/docs/writing-docs/autodocs
	tags: ['autodocs'],
	parameters: {
		// More on how to position stories at: https://storybook.js.org/docs/configure/story-layout
		layout: 'centered',
	},
};

type Story = StoryObj<typeof DateTimeInput>;

export const BasicStory: Story = {
	args: {
		UniqueName: 'date-time-input-1',
		Label: 'Date'
	}
};
