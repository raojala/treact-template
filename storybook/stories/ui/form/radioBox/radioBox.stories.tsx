import type { StoryObj } from '@storybook/react';
import React from 'react';
import { RadioBox } from 'ui';

export default {
	title: 'FORM/RadioBox',
	component: RadioBox,
	// This component will have an automatically generated Autodocs entry: https://storybook.js.org/docs/writing-docs/autodocs
	tags: ['autodocs'],
	parameters: {
		// More on how to position stories at: https://storybook.js.org/docs/configure/story-layout
		layout: 'centered',
	},
};

type Story = StoryObj<typeof RadioBox>;

export const BasicStory: Story = {
	render: () => (
		<>
			<RadioBox UniqueName='radiobox-1' Label='radio-1' Group='rg' />
			<RadioBox UniqueName='radiobox-2' Label='radio-2' Group='rg' />
		</>
	)
};