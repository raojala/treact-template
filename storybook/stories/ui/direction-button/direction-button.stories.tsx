import type { StoryObj } from '@storybook/react';
import { DirectionBtn } from 'ui';

export default {
	title: 'UI/DirectionBtn',
	component: DirectionBtn,
	// This component will have an automatically generated Autodocs entry: https://storybook.js.org/docs/writing-docs/autodocs
	tags: ['autodocs'],
	parameters: {
		// More on how to position stories at: https://storybook.js.org/docs/configure/story-layout
		layout: 'centered',
	},
};

type Story = StoryObj<typeof DirectionBtn>;

export const NextButton: Story = {
	parameters: {
		docs: {
			description: {
				story: 'Button style that indicates primary button',
			},
		}
	},
	args: {
		Text: 'Next Button',
		Direction: 'Next',
		Feeling: 'positive',
		OnClick: () => alert('button clicked')
	}
};


export const PreviousButton: Story = {
	parameters: {
		docs: {
			description: {
				story: 'Button style that indicates primary button',
			},
		}
	},
	args: {
		Text: 'Previous Button',
		Direction: 'Prev',
		Feeling: 'negative',
		OnClick: () => alert('button clicked')
	}
};


export const SecondaryNextButton: Story = {
	parameters: {
		docs: {
			description: {
				story: 'Button style that indicates primary button',
			},
		}
	},
	args: {
		Text: 'Previous Button',
		Direction: 'Next',
		Feeling: 'secondary',
		OnClick: () => alert('button clicked')
	}
};


export const PrimaryPreviousButton: Story = {
	parameters: {
		docs: {
			description: {
				story: 'Button style that indicates primary button',
			},
		}
	},
	args: {
		Text: 'Previous Button',
		Direction: 'Prev',
		Feeling: 'primary',
		OnClick: () => alert('button clicked')
	}
};