import type { StoryObj } from '@storybook/react';
import { Row } from 'layout';
import React from 'react';

export default {
	title: 'Layout/Row',
	component: Row,
	// This component will have an automatically generated Autodocs entry: https://storybook.js.org/docs/writing-docs/autodocs
	tags: ['autodocs'],
	parameters: {
		// More on how to position stories at: https://storybook.js.org/docs/configure/story-layout
		layout: 'centered',
	},
};

type Story = StoryObj<typeof Row>;

export const ThreeItems: Story = {
	args: {
		children: <><p>Item-1</p><p>Item-2</p><p>Item-3</p></>,
	},
};