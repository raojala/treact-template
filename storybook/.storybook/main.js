import { dirname, join } from "path";

import MiniCssExtractPlugin from 'mini-css-extract-plugin';

/**
 * This function is used to resolve the absolute path of a package.
 * It is needed in projects that use Yarn PnP or are set up within a monorepo.
 */
function getAbsolutePath(value) {
	return dirname(require.resolve(join(value, "package.json")));
}

/** @type { import('@storybook/react-webpack5').StorybookConfig } */
const config = {
	stories: [
		"../stories/**/*.mdx",
		"../stories/**/*.stories.@(js|jsx|mjs|ts|tsx)",
	],
	addons: [
		getAbsolutePath("@storybook/addon-links"),
		getAbsolutePath("@storybook/addon-essentials"),
		getAbsolutePath("@storybook/addon-onboarding"),
		getAbsolutePath("@storybook/addon-interactions"),
		getAbsolutePath("@storybook/addon-a11y"),
	],
	framework: {
		name: getAbsolutePath("@storybook/react-webpack5"),
		options: {
			builder: {
				useSWC: true,
			},
		},
	},
	docs: {
		autodocs: "tag",
	},
	webpackFinal: async (config) => {
		config.plugins.push(
			new MiniCssExtractPlugin({
				filename: '[name].css?[contenthash]',
			})
		);
		config.module.rules.push({
			test: /\.s[ac]ss$/,
			use: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader'],
			exclude: /node_modules/,
		});
		config.module.rules.push({
			test: /\.tsx?$/,
			use: 'ts-loader',
			exclude: /node_modules/,
		});
		return config;
	},
};
export default config;
