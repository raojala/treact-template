import { Lang } from 'localization';

describe('localization.cy.ts', () => {

	it('test english language object', () => {
		const word = Lang.Get('TestWord');
		assert(word === 'testword', 'got unexpected message');
	});

	it('test finnish language object', () => {
		Lang.SetLanguage('fi');
		const word = Lang.Get('TestWord');
		assert(word === 'testisana', 'got unexpected message');
	});
});