import { Row } from 'layout';
import React from 'react';

it('mounts', () => {
	cy.mount(<Row><></></Row>);
	cy.get('.Row').should('exist');
});