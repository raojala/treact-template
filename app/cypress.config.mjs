import { defineConfig } from 'cypress';
import webpackConfig from './webpack.config.mjs';

export default defineConfig({
	e2e: {
		// setupNodeEvents(on, config) {
		// 	// implement node event listeners here
		// },
	},

	component: {
		devServer: {
			framework: 'react',
			bundler: 'webpack',
			webpackConfig: {
				...webpackConfig,
				devServer: null
			},
		},
	},
});
