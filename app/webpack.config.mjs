import CopyPlugin from 'copy-webpack-plugin';
import HtmlWebpackPlugin from 'html-webpack-plugin';
import MiniCssExtractPlugin from 'mini-css-extract-plugin';
import path from 'path';

import { fileURLToPath } from 'url';
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);

const outputPath = './dist';
const entryFile = './src/main.tsx';
const indexTemplatePath = './html/index.html';
const faviconPath = './html/favicon.svg';
const publicFolder = './assets';

const webpackConfig = {
	entry: entryFile,
	output: {
		filename: '[name].js?[contenthash]',
		path: path.resolve(__dirname, outputPath),
		clean: true,
		publicPath: '/'
	},
	plugins: [
		new MiniCssExtractPlugin({
			filename: '[name].css?[contenthash]',
		}),
		new HtmlWebpackPlugin({
			filename: 'index.html',
			template: indexTemplatePath,
			favicon: faviconPath,
			publicPath: '/'
		}),
		new CopyPlugin({
			patterns: [
				{ from: publicFolder, to: '' },
			],
		}),
	],
	module: {
		rules: [
			{
				test: /\.s[ac]ss$/,
				use: [MiniCssExtractPlugin.loader, 'css-loader', 'sass-loader'],
				exclude: /node_modules/,
			},
			{
				test: /\.tsx?$/,
				use: 'ts-loader',
				exclude: /node_modules/,
			},
			{
				test: /\.(png|jpg|gif|svg)$/,
				type: 'asset/resource',
			}
		]
	},
	resolve: {
		extensions: ['.tsx', '.ts', '.js'],
	},
	optimization: {
		splitChunks: {
			chunks: 'all',
		},
	},
	devServer: {
		static: outputPath,
		host: 'localhost',
		port: '3000',
		https: false,
	},
};

export default webpackConfig;