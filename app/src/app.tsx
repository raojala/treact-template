import React from 'react';
import logo from '../assets/icons/manifest-icon-512.maskable.png';
import './styles/styles.scss';

export const App = (): JSX.Element => {
	return <>
		<img src={logo} className="App-logo" alt="logo" />
		<p>Official Softaloossi typescript react project template</p>
		<p>To start developing</p>
	</>;
};
