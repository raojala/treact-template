# tReact template
### About

|||
|:--:|:--:|
|Author|Rami Ojala|
|Lisence|MIT|
|Last Updated|05/12/2022|

Just a simple quick start project template for typescript and react.

### Dependencies

|||
|:--:|:--:|
|NPM|8.19.2|
|Node|16.18.0|

## Noteworty NPM packages

- axios
- lodash-es
- react-hook-form
- react-query
- react-router
- react-toastify
- zustand
- zod

## NPM scripts to get started

```
"scripts": {
    "dev": "concurrently --kill-others \"npm run watch\" \"npm run serve\"",
    "watch": "npx webpack --config webpack.config.js --mode=development --watch --devtool inline-source-map",
    "serve": "npx webpack serve --open --config webpack.config.js --mode=development --devtool inline-source-map",
    "build": "npx webpack --config webpack.config.js --mode=production"
},
```

**dev** is used to watch changes and serve files (with hot reload). underneath calls **watch** and **serve**  
**build** is used to build ready webpage