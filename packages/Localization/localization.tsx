import { z } from 'zod';

const translationsSchema = z.object({
	'TestWord': z.string().min(5),
}).strict();
type Translation = z.infer<typeof translationsSchema>
type Translations = {
	[languageKey: string]: Translation;
}
type TranslationKeys = keyof Translation;

const languagesSchema = z.object({
	'en': translationsSchema,
	'fi': translationsSchema,
}).strict();
type Languages = z.infer<typeof languagesSchema>
type LanguagesKeys = keyof Languages;

class Language {
	private localization: Translations;
	private selectedLang: Translation;

	constructor() {
		this.localization = languagesSchema.parse({
			'en': {
				'TestWord': 'testword',
			},
			'fi': {
				'TestWord': 'testisana',
			}
		}) as Translations;

		this.selectedLang = this.localization['en'];
	}

	SetLanguage = (key: LanguagesKeys) => {
		this.selectedLang = this.localization[key];
	};

	Get(key: TranslationKeys) {
		return this.selectedLang[key];
	}
}
const l = new Language();
export const Lang = l;