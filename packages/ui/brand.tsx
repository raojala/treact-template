export type Feeling = 'primary' | 'secondary' | 'negative' | 'positive';
