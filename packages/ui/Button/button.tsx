import React from 'react';
import { Feeling } from '..';
import './button.scss';

type ButtonProps = {
	Text: string
	OnClick: () => void
	Feeling: Feeling
	DataTestId?: string
	AdditionalCSS?: string
}

export const Button = (props: ButtonProps): JSX.Element => {
	const { Text, OnClick, Feeling, DataTestId, AdditionalCSS } = props;

	const basicStyles = 'button';
	const styles = basicStyles
		.concat(' ', Feeling)
		.concat(AdditionalCSS ? ' ' + AdditionalCSS : '');

	return <button data-testid={DataTestId} className={styles} onClick={OnClick}>{Text}</button>;
};