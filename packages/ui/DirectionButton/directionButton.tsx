import React from 'react';
import { Feeling } from '..';
import './direction-button.scss';

type DirectionBtnProps = {
	Text: string
	OnClick: () => void
	Direction: 'Prev' | 'Next'
	Feeling: Feeling
	DataTestId?: string
	AdditionalCSS?: string
}

export const DirectionBtn = (props: DirectionBtnProps): JSX.Element => {
	const { Text, OnClick, Direction, Feeling, DataTestId, AdditionalCSS } = props;

	const basicStyles = Direction == 'Next' ? 'btn-next' : 'btn-previous';
	const styles = 'direction-btn ' + basicStyles
		.concat(' ', Feeling)
		.concat(AdditionalCSS ? ' ' + AdditionalCSS : '');

	return <button data-testid={DataTestId} className={styles} onClick={OnClick}><div className='text'>{Text}</div></button>;
};