import React from 'react';
import './passwordInput.scss';

type PasswordInputProps = {
	UniqueName: string
	Label: string
	DataTestId?: string
	Placeholder?: string
	AdditionalCSS?: string
}

export const PasswordInput = (props: PasswordInputProps): JSX.Element => {
	const { UniqueName, Label, DataTestId, Placeholder, AdditionalCSS } = props;

	return (
		<label className='password-input' htmlFor={UniqueName}>{Label}<br />
			<input id={UniqueName} className={`password-input ${AdditionalCSS}`} type="password" placeholder={Placeholder} data-testid={DataTestId} />
		</label>
	);
};