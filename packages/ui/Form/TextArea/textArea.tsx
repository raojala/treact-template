import React from 'react';
import './textArea.scss';

type TextAreaProps = {
	UniqueName: string
	Label: string
	DataTestId?: string
	Placeholder?: string
	AdditionalCSS?: string
}

export const TextArea = (props: TextAreaProps): JSX.Element => {
	const { UniqueName, Label, DataTestId, Placeholder, AdditionalCSS } = props;

	return (
		<label className='textarea-input' htmlFor={UniqueName}>{Label}<br />
			<textarea id={UniqueName} className={`textarea-input ${AdditionalCSS}`} placeholder={Placeholder} data-testid={DataTestId} />
		</label>
	);
};