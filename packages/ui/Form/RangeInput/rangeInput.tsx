import React, { useState } from 'react';
import './rangeInput.scss';

type RangeInputProps = {
	UniqueName: string
	Label: string
	Min: number
	Max: number
	DataTestId?: string
	AdditionalCSS?: string
}

// todo:: continue this
export const RangeInput = (props: RangeInputProps): JSX.Element => {
	const { UniqueName, Label, Min, Max, DataTestId, AdditionalCSS } = props;

	const [rangeValue, setRangeValue] = useState(Min + '');

	const basicStyles = 'custom-range';
	const styles = basicStyles
		.concat(AdditionalCSS ? ' ' + AdditionalCSS : '');

	return (
		<label>{Label} <br />
			<input id={UniqueName} className={styles} type="range"
				data-testid={DataTestId}
				min={Min} max={Max}
				value={rangeValue} onChange={(e) => setRangeValue(e.target.value)} /><br />
			<output id={UniqueName} className="range-value">{rangeValue}</output>
		</label>
	);
};