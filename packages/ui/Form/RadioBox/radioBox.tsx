import React, { useState } from 'react';
import './radioBox.scss';

type RadioBoxProps = {
	UniqueName: string
	Label: string
	Group: string
	DataTestId?: string
	AdditionalCSS?: string
}

export const RadioBox = (props: RadioBoxProps): JSX.Element => {
	const { UniqueName, Label, Group, DataTestId, AdditionalCSS } = props;

	const [checked, setChecked] = useState(false);
	const handleChange = (e: React.FormEvent<HTMLInputElement>) => {
		setChecked(e.currentTarget.checked);
	};

	const basicStyles = 'checkbox';
	const styles = basicStyles
		.concat(AdditionalCSS ? ' ' + AdditionalCSS : '');

	return (
		<label className={`container ${checked ? ' check-hover-cut' : ''}`}>{Label}
			<input id={UniqueName} className={styles} type="radio" name={Group} data-testid={DataTestId} onChange={handleChange} checked={checked} />
			<span className="checkmark"></span>
		</label>
	);
};