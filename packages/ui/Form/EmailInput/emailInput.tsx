import React from 'react';
import './emailInput.scss';

type EmailInputProps = {
	UniqueName: string
	Label: string
	DataTestId?: string
	Placeholder?: string
	AdditionalCSS?: string
}

export const EmailInput = (props: EmailInputProps): JSX.Element => {
	const { UniqueName, Label, DataTestId, Placeholder, AdditionalCSS } = props;

	return (
		<label className='email-input' htmlFor={UniqueName}>{Label}<br />
			<input id={UniqueName} className={`email-input ${AdditionalCSS}`} type="email" placeholder={Placeholder} data-testid={DataTestId} />
		</label>
	);
};