import React from 'react';
import './textInput.scss';

type TextInputProps = {
	UniqueName: string
	Label: string
	DataTestId?: string
	Placeholder?: string
	AdditionalCSS?: string
}

export const TextInput = (props: TextInputProps): JSX.Element => {
	const { UniqueName, Label, DataTestId, Placeholder, AdditionalCSS } = props;

	return (
		<label className='text-input' htmlFor={UniqueName}>{Label}<br />
			<input id={UniqueName} className={`text-input ${AdditionalCSS}`} type="text" placeholder={Placeholder} data-testid={DataTestId} />
		</label>
	);
};