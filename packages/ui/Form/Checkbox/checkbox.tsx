import React, { useState } from 'react';
import './checkbox.scss';

type CheckBoxProps = {
	UniqueName: string
	Label: string
	DataTestId?: string
	AdditionalCSS?: string
}

export const Checkbox = (props: CheckBoxProps): JSX.Element => {
	const { UniqueName, Label, DataTestId, AdditionalCSS } = props;

	const [checked, setChecked] = useState(false);
	const handleChange = (e: React.FormEvent<HTMLInputElement>) => {
		setChecked(e.currentTarget.checked);
	};

	const basicStyles = 'checkbox';
	const styles = basicStyles
		.concat(AdditionalCSS ? ' ' + AdditionalCSS : '');

	return (
		<label className={`container ${checked ? ' check-hover-cut' : ''}`}>{Label}
			<input id={UniqueName} className={styles} type="checkbox" data-testid={DataTestId} onChange={handleChange} checked={checked} />
			<span className="checkmark"></span>
		</label>
	);
};