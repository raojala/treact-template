import React from 'react';
import './dateTimeInput.scss';

type DateTimeInputProps = {
	UniqueName: string
	Label: string
	DataTestId?: string
	AdditionalCSS?: string
}

export const DateTimeInput = (props: DateTimeInputProps): JSX.Element => {
	const { UniqueName, Label, DataTestId, AdditionalCSS } = props;

	return (
		<label className='date-time-input' htmlFor={UniqueName}>{Label}<br />
			<input id={UniqueName} className={`date-time-input ${AdditionalCSS}`} type="datetime-local" data-testid={DataTestId} />
		</label>
	);
};