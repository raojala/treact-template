import React from 'react';
import './numberInput.scss';

type NumberInputProps = {
	UniqueName: string
	Label: string
	DataTestId?: string
	Placeholder?: string
	AdditionalCSS?: string
}

export const NumberInput = (props: NumberInputProps): JSX.Element => {
	const { UniqueName, Label, DataTestId, Placeholder, AdditionalCSS } = props;

	return (
		<label className='numberInput' htmlFor={UniqueName}>{Label}<br />
			<input id={UniqueName} className={`numberInput ${AdditionalCSS}`} type="number" placeholder={Placeholder} data-testid={DataTestId} pattern="[0-9]*" />
		</label>
	);
};