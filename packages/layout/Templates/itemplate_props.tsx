export type TemplateProps = {
	menu: JSX.Element,
	content: JSX.Element
}