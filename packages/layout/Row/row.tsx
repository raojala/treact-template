/**
 * Displays all components inside left to right
 */
import React from 'react';
import './row.scss';

export type RowProps = {
	children: JSX.Element | Array<JSX.Element>,
	AdditionalCSS?: string
}

export const Row = (props: RowProps): JSX.Element => {
	const { children, AdditionalCSS } = props;

	return (
		<div className={`row ${AdditionalCSS}`} >
			{children}
		</div>
	);
};
