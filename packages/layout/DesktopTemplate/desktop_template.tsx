/**
 * Basic layout of the webpage
 */
import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import { ToastContainer } from 'react-toastify';
import { TemplateProps } from '../Templates/itemplate_props';
import { Row } from './../Row/row';
import './desktop_template.scss';

export const DesktopTemplate = (props: TemplateProps): JSX.Element => {
	const { menu, content } = props;

	return (
		<>
			<ToastContainer
				position="top-right"
				newestOnTop={true}
				closeOnClick={true}
				autoClose={false}
				rtl={false}
				pauseOnFocusLoss={false}
				draggable={false}
			/>
			<div className="desktop">
				<BrowserRouter>
					<Row AdditionalCSS="page-Content">
						{menu}
						{content}
					</Row>
				</BrowserRouter>
			</div>
		</>
	);
};
