/**
 * Displays all components inside top to bottom
 */
import React from 'react';
import './column.scss';

type ColumnProps = {
	children: JSX.Element | Array<JSX.Element>,
	AdditionalCSS?: string
}

export const Column = (props: ColumnProps): JSX.Element => {
	const { children, AdditionalCSS } = props;

	return (
		<div className={`column ${AdditionalCSS}`}>
			{children}
		</div>
	);
};