/**
 * Basic layout of the webpage
 */
import React from 'react';
import { BrowserRouter } from 'react-router-dom';
import { ToastContainer } from 'react-toastify';
import { TemplateProps } from '../Templates/itemplate_props';
import { Column } from './../Column/column';
import './mobile_template.scss';

export const MobileTemplate = (props: TemplateProps): JSX.Element => {
	const { menu, content } = props;

	return (
		<>
			<ToastContainer
				position="bottom-center"
				newestOnTop={false}
				closeOnClick={true}
				autoClose={false}
				rtl={false}
				pauseOnFocusLoss={false}
				draggable={false}
			/>
			<div className="mobile">
				<BrowserRouter>
					<Column AdditionalCSS="page-Content">
						{menu}
						{content}
					</Column>
				</BrowserRouter>
			</div>
		</>
	);
};
